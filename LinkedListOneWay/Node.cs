﻿namespace LinkedListOneWay
{
    internal class Node<T>
    {
        public T NodeValue { get; set; }
        public Node<T> Next { get; set; }
    }
}