﻿using System;
using System.Linq;

namespace LinkedListOneWay
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var list = new LinkedList<int>();
            foreach (int i in Enumerable.Range(1, 10))
            {
                list.addToStart(i);
            }

            list.Head = list.Reverse2();

            var aNode = list.Head;
            while (aNode != null)
            {
                Console.WriteLine(aNode.NodeValue);
                aNode = aNode.Next;
            }
        }
    }
}