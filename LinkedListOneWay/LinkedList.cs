﻿namespace LinkedListOneWay
{
    internal class LinkedList<T>
    {
        public Node<T> Head { get; set; }
        //public Node<T> Tail { get; set; }

        public void addToEnd(T value)
        {
            var newNode = new Node<T>() { NodeValue = value, Next = null };

            if (Head == null)
                Head = newNode;
            else
            {
                var currentNode = Head;
                while (currentNode.Next != null)
                {
                    currentNode = currentNode.Next;
                }
                currentNode.Next = newNode;
            }
        }

        public Node<T> Reverse()
        {
            Node<T> pre = null;
            var current = Head;
            Node<T> next = null;
            while (current != null)
            {
                next = current.Next;

                current.Next = pre;

                pre = current;

                current = next;
            }
            return pre;
        }

        public Node<T> Reverse2()
        {
            var pre = default(Node<T>);
            var cur = Head;
            var next = default(Node<T>);

            while (cur != null)
            {
                next = cur.Next;
                cur.Next = pre;
                pre = cur;
                cur = next;
            }
            return pre;
        }

        public void addToStart(T t)
        {
            if (Head == null)
            {
                Head = new Node<T> { NodeValue = t, Next = null };
            }
            else
            {
                var newNode = new Node<T>() { NodeValue = t, Next = Head };

                Head = newNode;
            }
        }
    }
}