﻿using System;
using System.Threading.Tasks;

namespace Reborn
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //BlockingMethod();
            //NonBlockingConcurrent();
            NonBlockingParallel();
        }

        private static void BlockingMethod()
        {
            Task1();
            Task2();
            UserInput();

            Console.WriteLine("App exit in 4 seconds");
            Task.Delay(4000).Wait();
        }

        private static void NonBlockingConcurrent()
        {
            Task1Async();
            Task2Async();
            UserInput();
        }

        private static void NonBlockingParallel()
        {
            Task.Factory.StartNew(Task1);
            Task.Factory.StartNew(Task2);
            UserInput();
        }

        private static async void Task2Async()
        {
            await Task.Delay(2000);
            Console.WriteLine("task 2");
        }

        private static async void Task1Async()
        {
            await Task.Delay(2000);
            Console.WriteLine("task 1");
        }

        private static void UserInput()
        {
            Console.WriteLine("enter data");
            string data = Console.ReadLine();
            Console.WriteLine($"you entered {data}");
        }

        private static void Task2()
        {
            Task.Delay(2000).Wait();
            Console.WriteLine("task 2");
        }

        private static void Task1()
        {
            Task.Delay(1000).Wait();
            Console.WriteLine("task 1");
        }
    }
}