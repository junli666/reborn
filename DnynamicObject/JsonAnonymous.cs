﻿using Newtonsoft.Json;

namespace DynamicObject
{
    internal class JsonAnonymous
    {
        public static void Demo()
        {
            var ninjaAnonymous = new { friendlyName = string.Empty, fitnessLevel = string.Empty };
            var ninja = @"{'friendlyName': 'Nagano','fitnessLevel': 'super'}";
            var result = JsonConvert.DeserializeAnonymousType(ninja, ninjaAnonymous);
            System.Console.WriteLine(result.friendlyName);
        }
    }
}