﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace DynamicObject
{
    internal class JsonTime
    {
        private static Ninja dd = new Ninja()
        {
            Courses = new List<string> { "Jump", "Run" },
            FitnessLevel = "Super Man",
            FriendlyName = "Drew",
            DOB = new DateTime(1989, 1, 6)
        };

        public static void Demo()
        {
            Console.WriteLine("Default");
            string dateDefault = JsonConvert.SerializeObject(dd);
            Console.WriteLine(dateDefault);
        }

        public static void DemoMSFormat()
        {
            Console.WriteLine("MS Format");
            JsonSerializerSettings jf = new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
            string dateDefault = JsonConvert.SerializeObject(dd, jf);
            Console.WriteLine(dateDefault);
        }

        public static void DemoJsFormat()
        {
            Console.WriteLine("JS Format");
            string dateDefault = JsonConvert.SerializeObject(dd, new JavaScriptDateTimeConverter());
            Console.WriteLine(dateDefault);
        }

        public static void DemoCustomFormat()
        {
            Console.WriteLine("Custom Format");
            JsonSerializerSettings jf = new JsonSerializerSettings { DateFormatString = "dd/MMM/yyyy" };

            string dateDefault = JsonConvert.SerializeObject(dd, jf);
            Console.WriteLine(dateDefault);
        }
    }
}