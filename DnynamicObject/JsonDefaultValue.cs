﻿using Newtonsoft.Json;
using System;

namespace DynamicObject
{
    internal class JsonDefaultValue
    {
        public static void Demo()
        {
            Ninja a = new Ninja { FriendlyName = "default", DOB = DateTime.Now };

            a.FriendlyName = "FFF";
            Console.WriteLine("----DefaultValueHandling.Ignore----");
            var result = JsonConvert.SerializeObject(a, new JsonSerializerSettings()
            {
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Formatting = Formatting.Indented
            });

            Console.WriteLine(result);

            Console.WriteLine("----DefaultValueHandling.populate (not work)----");
            var result2 = JsonConvert.SerializeObject(a, new JsonSerializerSettings()
            {
                DefaultValueHandling = DefaultValueHandling.Populate,
                Formatting = Formatting.Indented
            });

            Console.WriteLine(result2);
        }
    }
}