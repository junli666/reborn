﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DynamicObject
{
    internal class JTextWriter
    {
        public static void demo()
        {
            Ninja jg = new Ninja()
            {
                Courses = new List<string> { "Jump", "Run" },
                FitnessLevel = "Super Woman",
                FriendlyName = "Jessie"
            };
            var serilizer = new JsonSerializer();
            //    serilizer.Formatting = Formatting.Indented;
            using (StreamWriter sw = new StreamWriter(@"..\..\noFormatting.json"))
            {
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    serilizer.Serialize(jw, jg);
                }
            }
        }

        public static void demoIndented()
        {
            Ninja jg = new Ninja()
            {
                Courses = new List<string> { "Jump", "Run" },
                FitnessLevel = "Super Woman",
                FriendlyName = "Jessie"
            };
            var serilizer = new JsonSerializer();
            serilizer.Formatting = Formatting.Indented;
            using (StreamWriter sw = new StreamWriter(@"..\..\indedeted.json"))
            {
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    serilizer.Serialize(jw, jg);
                }
            }
        }

        public static void demoIgnoreNull()
        {
            Ninja jg = new Ninja()
            {
                Courses = new List<string> { "Jump", "Run" },
                FitnessLevel = "Super Woman",
                FriendlyName = null
            };
            var serilizer = new JsonSerializer();
            serilizer.Formatting = Formatting.Indented;
            serilizer.NullValueHandling = NullValueHandling.Ignore;
            using (StreamWriter sw = new StreamWriter(@"..\..\ignoreNull.json"))
            {
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    serilizer.Serialize(jw, jg);
                }
            }
        }
    }
}