﻿using Newtonsoft.Json;

namespace DynamicObject.Model
{
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    //ignore JsonProperty chk JsonIgnore NonSerialized JsonIgnoreAttribute etc
    public class NinjaJsonOptOut
    {
        [JsonProperty(PropertyName = "HHH", Required = Required.Always)]  //no use as it ignores JsonProperty
        public int SkillLevel { get; set; }

        public string FriendlyName { get; set; }

        public string Alias { get; set; }

        [JsonIgnoreAttribute]
        public string NotRequired { get; set; }

        [JsonIgnore]
        public bool Happy;
    }
}