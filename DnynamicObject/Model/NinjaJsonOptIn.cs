﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DynamicObject.Model
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class NinjaJsonOptIn
    {
        [JsonProperty(PropertyName = "HHH", Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Skill SkillLevel { get; set; }

        [JsonProperty]
        public string FriendlyName { get; set; }

        [JsonProperty]
        public string Alias { get; set; }

        public string NotRequired { get; set; }
    }

    public enum Skill
    {
        novice,
        professional,
        expert
    }
}