﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DynamicObject
{
    internal class Ninja
    {
        [DefaultValue("default")]
        public string FriendlyName { get; set; }

        public List<string> Courses { get; set; }

        [DefaultValue("normal")]
        public string FitnessLevel { get; set; }

        public DateTime DOB { get; set; }
    }
}