﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DynamicObject
{
    public class RefJson
    {
        public static void Demo()
        {
            Ninja js = new Ninja()
            {
                Courses = new List<string> { "Jump", "Run" },
                FitnessLevel = "Super Woman",
                FriendlyName = "Jessie"
            };
            Ninja dd = new Ninja()
            {
                Courses = new List<string> { "Jump", "Run" },
                FitnessLevel = "Super Man",
                FriendlyName = "Drew"
            };

            var ninjas = new List<Ninja>() { js, dd, js };
            string jsonNinjaString = JsonConvert.SerializeObject(ninjas, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
            Console.WriteLine(jsonNinjaString); //last output will refer to first

            var serilizeNinja = JsonConvert.DeserializeObject<List<Ninja>>(jsonNinjaString);
            Console.WriteLine(serilizeNinja[0].FriendlyName);
        }
    }
}