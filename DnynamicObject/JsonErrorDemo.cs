﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace DynamicObject
{
    public class JsonErrorDemo
    {
        private static string DateString = @"['1980-01-01T00:00:00Z', '19751030']";

        public static void ShowError()
        {
            try
            {
                List<DateTime> result = JsonConvert.DeserializeObject<List<DateTime>>(DateString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void ShowErrorConvertRightOnes()
        {
            List<string> errors = new List<string>();
            JsonSerializerSettings jss = new JsonSerializerSettings
            {
                Error = delegate (object sender, ErrorEventArgs args)
                {
                    errors.Add(args.ErrorContext.Error.Message);
                    args.ErrorContext.Handled = true;
                },
                Converters = { new IsoDateTimeConverter() }
            };

            List<DateTime> result = JsonConvert.DeserializeObject<List<DateTime>>(DateString, jss);
            result.ForEach(c => { Console.WriteLine(c); });
        }

        public static void HandleErrorWithFunction()
        {
            //List<string> errors = new List<string>();
            JsonSerializerSettings jss = new JsonSerializerSettings()
            {
                Error = customFunction,
                Converters = { new IsoDateTimeConverter() }
            };
            List<DateTime> result = JsonConvert.DeserializeObject<List<DateTime>>(DateString, jss);
            result.ForEach(c => { Console.WriteLine(c); });
        }

        private static void customFunction(object sender, ErrorEventArgs args)
        {
            var currentError = args.ErrorContext.Error.Message;
            Console.WriteLine(currentError);
            args.ErrorContext.Handled = true;
        }
    }
}