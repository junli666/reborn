﻿using DynamicObject;
using DynamicObject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace DnynamicObject
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("   --Serialize--   ");
            dynamic ninja = new ExpandoObject();
            ninja.FriendlyName = "Jessie Graff";
            ninja.Coureses = new List<string> { "Run", "Jump" };
            ninja.FitnessLevel = "Super";
            string jsonAuthorDynamic = JsonConvert.SerializeObject(ninja, Formatting.Indented);
            Console.WriteLine(jsonAuthorDynamic);

            Console.WriteLine("-- Deserialize --");
            dynamic sasuke = JsonConvert.DeserializeObject(jsonAuthorDynamic);
            Console.WriteLine(sasuke.FitnessLevel);

            RefJson.Demo();

            //JsonErrorDemo.ShowError();
            //JsonErrorDemo.ShowErrorConvertRightOnes();
            //JsonErrorDemo.HandleErrorWithFunction();
            //JsonAnonymous.Demo();
            //JTextWriter.demo();
            //JTextWriter.demoIndented();
            //JTextWriter.demoIgnoreNull();
            //JsonReaderText.demo();
            // JsonTime.Demo();
            //JsonTime.DemoCustomFormat();
            //JsonDefaultValue.Demo();

            // optin test
            NinjaJsonOptIn oi = new NinjaJsonOptIn() { Alias = "DD", FriendlyName = "Drew", NotRequired = "Should not show", SkillLevel = Skill.expert };

            Console.WriteLine(JsonConvert.SerializeObject(oi));

            ////optout test
            //NinjaJsonOptIn oo = new NinjaJsonOptIn() { Alias = "DD", FriendlyName = "Drew", NotRequired = "Should not show", SkillLevel = 9 };

            //Console.WriteLine(JsonConvert.SerializeObject(oo));
        }
    }
}