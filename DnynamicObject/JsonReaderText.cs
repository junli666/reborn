﻿using Newtonsoft.Json;
using System.IO;

namespace DynamicObject
{
    internal class JsonReaderText
    {
        public static void demo()
        {
            string text = @"{
                   'FriendlyName': 'Jessie',
                   'Courses': [
                   'Jump',
                   'Run'
                   ],
                   'FitnessLevel': 'Super Woman'
                   }";
            JsonTextReader jr = new JsonTextReader(new StringReader(text));
            while (jr.Read())
            {
                if (jr.Value != null)
                    System.Console.WriteLine("Token: {0}, Value: {1}", jr.TokenType, jr.Value);
                else
                    System.Console.WriteLine("Token: {0} ", jr.TokenType);
            }
        }
    }
}