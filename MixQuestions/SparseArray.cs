﻿namespace MixQuestions
{
    internal class SparseArray
    {
        public static int[] matchingStrings(string[] strings, string[] queries)
        {
            int[] result = new int[queries.Length];
            var counter = 0;
            foreach (var i in queries)
            {
                var count = 0;

                foreach (var j in strings)
                {
                    if (j.CompareTo(i) == 0)
                        count++;
                }
                result[counter++] = count;
            }
            return result;
        }
    }
}