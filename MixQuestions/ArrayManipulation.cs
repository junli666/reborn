﻿using System;
using System.Collections.Generic;

namespace MixQuestions
{
    internal class ArrayManipulation
    {
        public static long arrayManipulation2(int n, int[][] queries)
        {
            List<int> myArray = new List<int>();
            for (int i = 0; i < n; i++)
            {
                myArray.Add(0);
            }
            int max = 0;
            for (int i = 0; i < queries.GetLength(0); i++)
            {
                int start = queries[i][0];
                int end = queries[i][1];
                int valueToAdd = queries[i][2];
                Console.WriteLine(start + " " + end + " " + valueToAdd);
                for (int j = 0; j < myArray.Count; j++)
                {
                    if (j >= start && j <= end)
                    {
                        myArray[j] += valueToAdd;
                        if (myArray[j] > max)
                        {
                            max = myArray[j];
                        }
                    }
                }
            }
            Console.WriteLine(max);
            return max;
        }

        public static long arrayManipulation(int n, int[][] queries)
        {
            List<int> al = new List<int>();
            for (int i = 0; i < n; i++)
            {
                al.Add(0);
            }
            var myArray = al.ToArray();
            int max = 0;
            for (int i = 0; i < queries.GetLength(0); i++)
            {
                int start = queries[i][0];
                int end = queries[i][1];
                int valueToAdd = queries[i][2];
                Console.WriteLine(start + " " + end + " " + valueToAdd);
                for (int j = start; j < myArray.Length && j <= end; j++)
                {
                    if (j >= start && j <= end)
                    {
                        myArray[j] += valueToAdd;
                    }
                }
            }
            for (int q = 0; q < myArray.Length; q++)
            {
                if (myArray[q] > max)
                    max = myArray[q];
            }
            return max;
        }
    }
}