﻿using System.Collections.Generic;

namespace MixQuestions
{
    internal class TwoSum
    {
        private readonly int _sumToCheck;
        private readonly int[] _inputArray;
        public List<string> resultProve;

        public TwoSum(int[] arrayToCheck, int sumToCheck)
        {
            _sumToCheck = sumToCheck;
            _inputArray = arrayToCheck;
            resultProve = new List<string>();
        }

        public bool TwoSumCheck()
        {
            bool returnvalue = false;
            for (int i = 0; i < _inputArray.Length; i++)
            {
                for (int j = i + 1; j < _inputArray.Length; j++)
                {
                    if (_inputArray[i] + _inputArray[j] == _sumToCheck)
                        resultProve.Add(_inputArray[i] + " " + _inputArray[j]);
                }
            }
            if (resultProve.Count > 0)
                returnvalue = true;
            return returnvalue;
        }
    }
}