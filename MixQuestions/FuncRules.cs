﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MixQuestions
{
    public class FuncRules
    {
        public static bool isValid(string valueToCheck)
        {
            Func<string, bool>[] rules =
                {
                s=>s.Length>10 && s.Length<30,
                s=>s.StartsWith('a') || s.StartsWith('e'),
                s=>s.EndsWith(s[0])
            };

            return rules.All(r => r(valueToCheck) == true);
        }

        public static bool simpleIsValid(string valueToCheck)
        {
            Func<string, bool> isValid = m => !string.IsNullOrEmpty(m);
            bool result = isValid(valueToCheck);
            return result;
        }
    }
}