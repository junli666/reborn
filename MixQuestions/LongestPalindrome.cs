﻿using System;
using System.Collections.Generic;

namespace MixQuestions
{
    internal class LongestPalindrom
    {
        private readonly string inputString;

        public LongestPalindrom(string entry)
        {
            inputString = entry;
        }

        public List<string> AllPalindrome()
        {
            //assume it need at least 2 chars
            List<string> entries = new List<string>();
            for (int i = 0; i < inputString.Length; i++)
            {
                int step = 2;
                for (int j = step; j <= inputString.Length - i; j++)
                {
                    var testString = inputString.Substring(i, j);
                    if (isPalindrome(testString))
                    {
                        entries.Add(testString);
                    }
                }
            }
            return entries;
        }

        public bool isPalindrome(string value)
        {
            var charArray = value.ToCharArray();
            Array.Reverse(charArray);
            bool returnValue = false;
            if (value.ToLower().CompareTo(new string(charArray).ToLower()) == 0)
                returnValue = true;
            return returnValue;
        }
    }
}