﻿using System;
using System.Linq;

namespace MixQuestions
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //int[][] a = new int[3][];

            //a[0] = new int[3] { 1, 5, 3 };
            //a[1] = new int[3] { 4,8,7};
            //a[2] = new int[3] { 6,9,1};

            //ArrayManipulation.arrayManipulation(10, a);
            //int[] aa = new int[] { 2, 3, 4, 2, 3, 6, 8, 4, 5 };
            Console.WriteLine(FuncRules.isValid("abcdlkafjkajflkjaskfjy"));
        }

        private static double arrayMedian(int[] a, int start, int count)
        {
            int[] newArray = new int[count];
            Array.Copy(a, start, newArray, 0, count);
            Array.Sort(newArray);

            //var newArray = a.Where((b, i) => (i >= start && i < start + count)).OrderBy(b=>b).ToArray();

            if (newArray.Length % 2 == 1) return newArray[(newArray.Length + 1) / 2 - 1];
            else return ((double)newArray[newArray.Length / 2] + (double)newArray[newArray.Length / 2 - 1]) / 2;
        }

        private static void PalindromeCheck()
        {
            LongestPalindrom a = new LongestPalindrom("iFzyZqNAMFNHNmRBMzdzosvpFNcZuxnCzqBywRMXpwWwudMyHGMUTGjuyrXBbgTcsofKyEXlZUmeopPRqNjCnfeRszAuvdnuvXmeZGMflvFtkwRkQiydoIYvbXkLmjzsnKwFlsIprzMBPBVaQJRziyNmpjhwmhDQCeyKTxoWRLkcysxCJonXQihzBBOHibkQrgVLekAPTICkoClioqRmtxnGFvEtvvyBmPlKmdQGBWnsAyUfMjlZeSfjQfahWvZLcvyqkitAoSufHYTSTfCYpzPhVYsnDQgnZAnDZEkPqOnZGGTqrfjLNduDCgzKrGRFKxsqDrIvChuWzGBBXQrzuWEOCruHnRTXMkQYgTVBeVUdVzZuqiWLkbbANNKkFOLgOTpFXWEhJCStBUpnXFUysgmZZJYgTLnFgwRdBLPIJMRiyDiMqlkYzsbrbeLmtzSBAhiDBafHbnFbYudJJTQfplwFXXkNpcKVjIQlxPhcyvUSTbaQJaKgneUBsldpuIhYJYJgYRmvnnVcdUXmiwzejWAJcWxAFreQudUAHzezZNvtGkpJJXmGAIzblOPLVwZIcRktnTZDjizxSrQOdBVdmosKzMNyWpZWMvwKXwgYrPUjrfEKTTJdFUcSRXoUJLxIQqhWbYgzCJCoWJEyDYoliufQXgUoxYWSIsDgDVHpytbSjxEdYsmFYZvvxPKTiXaEiJNgHajiSxUHiphIXnHlvwzZbbcmLlIHRrWHvJViWrYAveHMDWpqwcXBmoEiMMMrdnJLxLqvjLImbKkxcntvwephOEIzkOUnRhBLvJthVffpyhXLEmxtvxlmtQMIVclRULddYjUurvEnxDPISrpyYmYSnKGodBcheTYCazyNumvmqLXCbsBREbAEQexctaEhEALNXkdhtfBMQZZDsAEnsZvaFMaEaYCDaDrolybFJHHrVKcPkFmazedytgaPtMxANMxZRUqhAOFFIGQiulPUBfttLChgWXfvUZOLQqOepwVZsuUCQQoyNJtabWZBVLPoaZDpLTtXUfKJJXySziAzpBTXgyfWDdJUKVjGdPKbJjQSFAyQXtOiJaAWvdlHEREbirDUWbmujRoOSNpFhkkrzOvawcQXLWaJBGrkDEuJaeCTJEMlovkqEswuqNgURWvsFurYzdfGnUxqbbxLCwiXHRxCWedDOrRdaidwlTfPXzqXqVzXntMwVAZzpYMGCvuAOryKCTIzzGIRPLouoRNcOXmCJelDsGUMdoLAthSSteXJafzYVoIKHtrZgvYSkiTCwvtDmHLqCxhTvNhDYYOJmflueXAIJMSKYfAxXiCwKSEcUeyGMgwHpbEeBfXebdmzfCshTKVOyzroAMlAokYsyyzolcwzVjSkkDwyrdZjEaWlbZGVBlPqoOdJUHHGxHriFVeHQMSXpGNiYatYLShdohLYQWQXxugZgkpCKNiKkqVtwyyHSuLdFzEQtdegovDihsOPPDdATjclqsEkocktKjSZSUCZjyqGyXZaSotBNfHqhdGWVezRhngyLfsKSXzvdPHsHsyczCdrvLWSjYGSwWEoNzIcxkhTPqQAUFCCJyMHflCGAIpaNtgpvgVCNkSF");
            var r = a.AllPalindrome();
            Console.WriteLine(r.OrderByDescending(s => s.Length).FirstOrDefault());
        }

        private static void sort3UniqueNumTest()
        {
            var a = new int[] { 1, 3, 2, 3, 3, 1, 2, 1, 1, 3 };
            ThreeUniqeNumberSort b = new ThreeUniqeNumberSort(a);
            foreach (var i in b.Sort())
            {
                Console.WriteLine(i);
            }
        }
    }
}