﻿using System.Collections.Generic;

namespace MixQuestions
{
    internal class ThreeUniqeNumberSort
    {
        private readonly int[] testData;

        public ThreeUniqeNumberSort(int[] testArray)
        {
            testData = testArray;
        }

        public IEnumerable<int> Sort()
        {
            var tupleOf1 = (baseValue: 1, count: 0);
            var tupleOf2 = (baseValue: 2, count: 0);
            var tupleOf3 = (baseValue: 3, count: 0);

            for (int i = 0; i < testData.Length; i++)
            {
                if (testData[i] == 1) tupleOf1.count++;
                if (testData[i] == 2) tupleOf2.count++;
                if (testData[i] == 3) tupleOf3.count++;
            }
            List<int> result = new List<int>();
            for (int i = 0; i < testData.Length; i++)
            {
                if (i < tupleOf1.count)
                {
                    result.Add(1);
                    continue;
                }
                if (i < (tupleOf1.count + tupleOf2.count))
                {
                    result.Add(2);
                    continue;
                }
                result.Add(3);
            }
            return result;
        }
    }
}