﻿using System;
using System.Linq;

namespace Sorting
{
    internal class InsertSort : ISorting
    {
        public void PrintMember<T>(T[] array) where T : IComparable
        {
            array.ToList().ForEach(c => { Console.WriteLine(c); });
        }

        public void Sort<T>(T[] array) where T : IComparable
        {
            for (int i = 1; i < array.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    if (array[j].CompareTo(array[j - 1]) < 0)
                    {
                        var t = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = t;
                    }
                }
            }
        }
    }
}