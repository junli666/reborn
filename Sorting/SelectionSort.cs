﻿using System;
using System.Linq;

namespace Sorting
{
    internal class SelectionSort : ISorting
    {
        public void PrintMember<T>(T[] array) where T : IComparable
        {
            array.ToList().ForEach(c => { Console.WriteLine(c); });
        }

        public void Sort<T>(T[] array) where T : IComparable
        {
            for (int i = 0; i < array.Length; i++)
            {
                var minIndex = i;
                for (int j = i; j < array.Length; j++)
                {
                    if (array[j].CompareTo(array[minIndex]) < 0)
                    {
                        minIndex = j;
                    }
                }
                var temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;
            }
        }
    }
}