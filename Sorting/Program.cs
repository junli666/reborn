﻿using System;

namespace Sorting
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("---------------------------------------------");

            var s = new int[] { 1, 4, 7, 9, 2, 0, 45, 8 };

            ISorting a = new SelectionSort();
            a.Sort(s);
            a.PrintMember(s);

            Console.WriteLine("---------------------------------------------");

            //s = new string[] { "hello", "this", "is", "test", "kj", "uhg" };
            s = new int[] { 1, 4, 7, 9, 2, 0, 45, 8 };
            ISorting b = new BubbleSort();
            b.Sort(s);
            b.PrintMember(s);

            Console.WriteLine("---------------------------------------------");

            s = new int[] { 1, 4, 7, 9, 2, 0, 45, 8 };
            ISorting i = new InsertSort();
            i.Sort(s);
            i.PrintMember(s);
        }
    }
}