﻿using System;

namespace Sorting
{
    public interface ISorting
    {
        void Sort<T>(T[] array) where T : IComparable;

        void PrintMember<T>(T[] array) where T : IComparable;
    }
}