﻿using System;
using System.Linq;

namespace Sorting
{
    internal class BubbleSort : ISorting
    {
        public void PrintMember<T>(T[] array) where T : IComparable
        {
            array.ToList().ForEach(c => { Console.WriteLine(c); });
        }

        public void Sort<T>(T[] array) where T : IComparable
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j].CompareTo(array[j + 1]) > 0)
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }
    }
}