﻿using System;

namespace DesignPatterns
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            PolymorphismTest(new Label2());
            PolymorphismTest(new TextBox2());
        }

        private static void PolymorphismTest(UIControl2 ui)
        {
            ui.draw();
        }

        private static void NullPatterTest()
        {
            var users = new NullPattern[]{
               new NullPattern(1,"Jun"),
               new NullPattern(2,"Lu"),
               new NullPattern(3,"Henry")
            };

            Console.WriteLine(NullPattern.FindUser(users, 5));
        }

        private static void BuilderPatternTest()
        {
            var bp = new BuilderPattern("Jun")
                .setAddress("Canterbury")
                .setCitizenShip("CHN")
                .setGender("m")
                .setPhoneNumber("12345");
            Console.WriteLine(bp);
        }

        private static void SingletonTest()
        {
            var bst = SingletonPattern.getInstance;
            bst.addTo();
            bst.addTo();
            Console.WriteLine(bst.ToString());
            var b2st = SingletonPattern.getInstance;
            b2st.addTo();  //2
            Console.WriteLine(bst.ToString()); //3
        }
    }
}