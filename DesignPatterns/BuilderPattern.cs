﻿namespace DesignPatterns
{
    public class BuilderPattern //as a user
    {
        private string address = "unknown";
        private string gender = "unknown";
        private string phoneNumber = "unknown";
        private string citizenShip = "unknown";
        public string name;

        public BuilderPattern(string name)
        {
            this.name = name;
        }

        public BuilderPattern setGender(string gender)
        {
            this.gender = gender;
            return this;
        }

        public BuilderPattern setPhoneNumber(string phoneNumber)
        {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public BuilderPattern setCitizenShip(string citizenShip)
        {
            this.citizenShip = citizenShip;
            return this;
        }

        public BuilderPattern setAddress(string address)
        {
            this.address = address;
            return this;
        }

        public override string ToString()
        {
            return $"{name,-20}:{gender,-5}:{citizenShip,-6} :{phoneNumber:-10}";
        }
    }
}