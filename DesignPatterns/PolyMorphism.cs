﻿using System;

namespace DesignPatterns
{
    internal class UIControl2
    {
        public virtual void draw()
        {
            Console.WriteLine("UIControl");
        }
    }

    internal class TextBox2 : UIControl2
    {
        public override void draw()
        {
            Console.WriteLine("draw textbox");
        }
    }

    internal class Label2 : UIControl2
    {
        public override void draw()
        {
            Console.WriteLine("draw label");
        }
    }
}