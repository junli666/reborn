﻿using System;

namespace DesignPatterns
{
    internal class UIControl
    {
        public void Enable()
        {
            Console.WriteLine("enabling");
        }

        public void Disable()
        {
            Console.WriteLine("Disabling");
        }
    }

    internal class TextBox : UIControl
    {
    }

    internal class Label : UIControl
    {
    }
}