﻿namespace DesignPatterns
{
    internal class FactoryPattern
    {
        public IMusician createMusician(string instrument)
        {
            if (string.Compare("piano", instrument.ToLower()) == 0)
            {
                return new Pianist();
            }
            else if (string.Compare("violin", instrument.ToLower()) == 0)
            {
                return new Violinist();
            }
            return new DefaultMusician();
        }
    }

    internal interface IMusician
    {
        string Play();
    }

    internal class DefaultMusician : IMusician
    {
        public string Play()
        {
            return "I can't play";
        }
    }

    internal class Violinist : IMusician
    {
        public string Play()
        {
            return "play violin";
        }
    }

    internal class Pianist : IMusician
    {
        public string Play()
        {
            return "play piano";
        }
    }
}