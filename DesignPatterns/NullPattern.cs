﻿using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns
{
    internal class NullPattern
    {
        private string name;
        private readonly int userid;

        public NullPattern(int id, string name)
        {
            userid = id;
            Authenticated = true;
            this.name = name;
        }

        public NullPattern()
        {
            Authenticated = false;
            this.name = "null";
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool Authenticated { get; set; }

        public override string ToString()
        {
            return name;
        }

        public static NullPattern FindUser(IEnumerable<NullPattern> users, int id)
        {
            var result = users.Where(u => u.userid == id).SingleOrDefault();
            return result == null ? new NullPattern() : result;
        }
    }
}