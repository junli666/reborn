﻿namespace DesignPatterns
{
    internal class Encaptulation
    {
    }

    //_balance is hidden and encaptualted in method
    internal class Account
    {
        private float _balance;

        public void Deposit(float amount)
        {
            if (amount > 0)
                _balance += amount;
        }

        public void WithDraw(float amount)
        {
            if (amount > 0 && _balance - amount >= 0)
                _balance -= amount;
        }
    }
}