﻿using System;

namespace DesignPatterns
{
    internal class Abstraction
    {
        public void Process()
        {
            var mailService = new Mailservice();
            mailService.SendEmail();
        }
    }

    internal class Mailservice
    {
        public void SendEmail()
        {
            Connect();
            Authenticate();
            Disconnect();
        }

        public void Connect()
        {
            Console.WriteLine("connect");
        }

        public void Authenticate()
        {
            Console.WriteLine("Authenticate");
        }

        public void Disconnect()
        {
            Console.WriteLine("disconnect");
        }
    }
}