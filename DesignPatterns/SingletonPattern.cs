﻿namespace DesignPatterns
{
    public sealed class SingletonPattern
    {
        private static SingletonPattern instance = null;
        private static readonly object padlock = new object();
        private int incValue;

        private SingletonPattern()
        {
            incValue = 0;
        }

        public static SingletonPattern getInstance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SingletonPattern();
                    }
                    return instance;
                }
            }
        }

        public void addTo()
        {
            incValue++;
        }

        public override string ToString()
        {
            return incValue + " ";
        }
    }
}