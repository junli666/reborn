using Moq;
using NUnit.Framework;

namespace NUnitTestWithMoq
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void SortTestReturn()
        {
            var m = new Mock<ISimpleClassForTest>();

            m.Setup(x => x.operate(It.IsAny<int>(), It.IsAny<int>())).Returns(9);

            ISimpleClassForTest itest = m.Object;

            var sut = itest.operate(8, 7);

            System.Console.WriteLine(sut);

            Assert.AreEqual(9, sut);
        }
    }
}