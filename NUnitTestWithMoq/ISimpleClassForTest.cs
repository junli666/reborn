﻿namespace NUnitTestWithMoq
{
    public interface ISimpleClassForTest
    {
        int operate(int a, int b);
    }
}