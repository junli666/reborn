﻿namespace NUnitTestWithMoq
{
    internal class SimpleClassForTestAdd : ISimpleClassForTest
    {
        public int operate(int a, int b)
        {
            return a + b;
        }
    }
}