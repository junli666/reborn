﻿using System;
using System.Linq;

namespace ExtensionAndFluentAPI
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var pivot = DateTime.Now;

            var TwoMinutesAgo = 2.Minutes().Ago();

            Console.WriteLine($"Pivot    :  {pivot.ToLongTimeString()}");

            Console.WriteLine($"2 min Ago:  {TwoMinutesAgo.ToLongTimeString()}");
        }
    }
}