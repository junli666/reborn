﻿using Microsoft.VisualBasic;
using System;

/// <summary>
/// </summary>
namespace ExtensionAndFluentAPI
{
    public static class Extensions
    {
        public static TimeSpan Minutes(this int value)
        {
            return new TimeSpan(0, 0, value, 0, 0);
        }

        public static TimeSpan Hours(this int value)
        {
            return new TimeSpan(0, value, 0, 0, 0);
        }

        public static TimeSpan Days(this int value)
        {
            return new TimeSpan(value, 0, 0, 0, 0);
        }

        public static DateTime Ago(this TimeSpan value)
        {
            return DateAndTime.Now - value;
        }
    }
}