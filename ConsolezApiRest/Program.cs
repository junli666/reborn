﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace ConsolezApiRest
{
    internal class Program
    {
        private static async Task Main()
        {
            IServiceProvider serviceProvider = ConfigureServices();

            var result = await serviceProvider.GetService<IReadEarthQuakeData>().ReadDataFromPeriod(DateTime.Parse("2019/12/25"), DateTime.Parse("2019/12/27"));
            await serviceProvider.GetService<IRenderEarthQuakeData>().Render(result);
        }

        private static IServiceProvider ConfigureServices()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection
                .AddTransient<IReadEarthQuakeData, ReadEarthQuakeData>()
                .AddTransient<IRenderEarthQuakeData, DisplayEarthQuakeOnConsole>();
            // you can chose to save it to file instead by implement the SaveEarthQuakeToFile below and replace it here.

            var serviceProvider = serviceCollection.BuildServiceProvider();
            return serviceProvider;
        }
    }
}