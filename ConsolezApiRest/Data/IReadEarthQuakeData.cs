﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsolezApiRest
{
    public interface IReadEarthQuakeData
    {
        Task<IEnumerable<EarthQuake>> ReadDataFromPeriod(DateTime from, DateTime to);
    }
}