﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsolezApiRest
{
    public interface IRenderEarthQuakeData
    {
        Task Render(IEnumerable<EarthQuake> earthQuakes);
    }
}