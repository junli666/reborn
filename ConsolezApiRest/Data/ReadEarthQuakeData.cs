﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ConsolezApiRest
{
    public class ReadEarthQuakeData : IReadEarthQuakeData
    {
        // http client should be used as a singleton
        // Ref: https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
        private static readonly HttpClient _httpClient;

        private const string BaseUrl = "https://earthquake.usgs.gov";

        static ReadEarthQuakeData()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(BaseUrl)
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IEnumerable<EarthQuake>> ReadDataFromPeriod(DateTime from, DateTime to)
        {
            using (HttpResponseMessage res = await _httpClient.GetAsync($"fdsnws/event/1/query?format=geojson&starttime={from.ToString("yyyy-MM-dd")}&endtime={to.ToString("yyyy-MM-dd")}"))
            {
                Console.WriteLine("GET");

                if (res.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success");
                    var contentStream = await res.Content.ReadAsStringAsync();
                    var response = JsonConvert.DeserializeObject<EarthQuakeResponse>(contentStream);
                    return response.Features.OrderByDescending(e => e.Properties.Mag).Take(30)
                                .Select(x => x.Properties);
                }
            }

            return Enumerable.Empty<EarthQuake>();
        }
    }
}