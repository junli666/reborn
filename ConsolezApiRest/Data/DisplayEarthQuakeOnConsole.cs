﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsolezApiRest
{
    public class DisplayEarthQuakeOnConsole : IRenderEarthQuakeData
    {
        public Task Render(IEnumerable<EarthQuake> earthQuakes)
        {
            foreach (var earthQuake in earthQuakes)
            {
                var display = $"Place:  {earthQuake.Place}\n"
                      + $"Mag:    {earthQuake.Mag}\n"
                      + $"Time:   {earthQuake.TimeUtc.ToString("dd/MM/yyyy")}\n";
                Console.WriteLine(display);
            }
            return Task.CompletedTask;
        }
    }
}