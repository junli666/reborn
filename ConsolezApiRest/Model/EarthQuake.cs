﻿using System;

namespace ConsolezApiRest
{
    public class EarthQuake
    {
        public string Mag { get; set; }
        public string Place { get; set; }
        public long Time { get; set; }
        public DateTime TimeUtc => DateTimeOffset.FromUnixTimeMilliseconds(Time).UtcDateTime;

        public override string ToString()
        {
            return "Place:  " + Place + "\n"
                  + "Mag:    " + Mag + "\n"
                  + "Time:   " + TimeUtc.ToString("dd/MM/yyyy hh:mm") + "\n";
        }
    }
}