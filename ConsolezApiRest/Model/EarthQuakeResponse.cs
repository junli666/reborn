﻿using System.Collections.Generic;

namespace ConsolezApiRest
{
    public class EarthQuakeResponse
    {
        public IEnumerable<EarthQuakeFeature> Features { get; set; }
    }
}