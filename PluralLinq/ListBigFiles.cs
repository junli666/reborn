﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace PluralLinq
{
    internal class ListBigFiles
    {
        public string path { get; set; }

        private Predicate<FileInfo> SizeBiggerThan1000 = f => f.Length > 1000;

        private bool SizeBiggerThan1000Method(FileInfo f)
        {
            return f.Length > 1000;
        }

        public void ListBiggest5FilesNormalWay()
        {
            FileInfo[] files = new DirectoryInfo(path).GetFiles();
            Array.Sort(files, new FileComparer());
            Array.Reverse(files);
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"{files[i].Name,-30}   :  {files[i].Length:N2}");
            }
        }

        public void ListBiggest5FilesLamdaWay()
        {
            IEnumerable<FileInfo> files = new DirectoryInfo(path).GetFiles().OrderByDescending(r => r.Length).Take(5);
            foreach (var t in files)
            {
                Console.WriteLine($"{t.Name,-30}   :  {t.Length:N2}");
            }
        }

        public void ListBiggest5FilesLinqWay()
        {
            IEnumerable<FileInfo> files = from file in (new DirectoryInfo(path).GetFiles())
                                          orderby file.Length descending
                                          select file
                ;

            foreach (var t in files.Take(5))
            {
                Console.WriteLine($"{t.Name,-30}   :  {t.Length:N2}");
            }
        }

        public void ListFilesBiggerThan1000()
        {
            IEnumerable<FileInfo> files = new DirectoryInfo(path).GetFiles().Where(r => SizeBiggerThan1000(r)).OrderByDescending(r => r.Length);
            foreach (var t in files.Take(5))
            {
                Console.WriteLine($"{t.Name,-30}   :  {t.Length:N2}");
            }
        }

        public void ListFilesBiggerThan1000Method()
        {
            IQueryable<FileInfo> files = new DirectoryInfo(path).GetFiles().Where(SizeBiggerThan1000Method).OrderByDescending(r => r.Length).AsQueryable();
            foreach (var t in files.Take(5))
            {
                Console.WriteLine($"{t.Name,-30}   :  {t.Length:N2}");
            }
        }

        public void ListFileByextensionMethod()
        {
            IEnumerable<FileInfo> files = new DirectoryInfo(path).Fileter();
            foreach (var t in files)
            {
                Console.WriteLine($"{t.Name,-30}   :  {t.Length:N2}");
            }
        }

        public class FileComparer : IComparer<FileInfo>
        {
            public int Compare([AllowNull] FileInfo x, [AllowNull] FileInfo y)
            {
                return x.Length.CompareTo(y.Length);
            }
        }
    }
}