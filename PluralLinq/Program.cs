﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PluralLinq
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ListBigFiles lbf = new ListBigFiles
            {
                path = @"C:\windows"
            };
            lbf.ListBiggest5FilesNormalWay();

            Console.WriteLine("-----------------------------");

            lbf.ListBiggest5FilesLamdaWay();

            Console.WriteLine("-----------------------------");

            lbf.ListBiggest5FilesLinqWay();

            Console.WriteLine("-----------------------------");

            lbf.ListFileByextensionMethod();

            Console.WriteLine("-----------------------------");

            lbf.ListFilesBiggerThan1000();
            Console.WriteLine("-----------------------------");

            lbf.ListFilesBiggerThan1000Method();

            var a = Enumerable.Range(1, 9999999).ToArray();
            Console.WriteLine(Algorithm.BinarySearch(a, -99999999, 0, a.Length));

            var b = new string[] { "a", "b", "c", "d", "e", "f", "g" };
            Console.WriteLine(Algorithm.BinarySearchGeneric(b, "c", 0, b.Length));
            //Console.WriteLine("a".CompareTo("b"));

            var bb = new int[] { 1, 8, 3, 5, 9, 2, 0, 4 };

            //Algorithm.InsertSort(bb);
            //Algorithm.BubbleSort(bb);
            //Algorithm.SelectionSort(bb);
            Algorithm.QuickSort(bb, 0, bb.Length - 1);

            Film fm = new Film();
            fm.getFilmsAfter1980MyWay();

            var car = ProcessCars("fuel.csv");
            var manufacturers = ProcessManufacturers("manufacturers.csv");

            var mQuery = manufacturers.Where(StartWithF);

            //easy
            var jointQuery = from c in car
                             join m in manufacturers on c.Manufacturer equals m.Name
                             orderby c.Manufacturer
                             select new
                             {
                                 c,
                                 m
                             };

            //hard to do with extension  method
            var jointQuery2 = car.Join(manufacturers,
                c => c.Manufacturer, m => m.Name,
                (c, m) => new { Car = c, Manufacturer = m }
                ).OrderByDescending(c => c.Car.Combined)
                .ThenBy(c => c.Car.Name);

            foreach (var t in mQuery)
            {
                Console.WriteLine(t);
            }

            var result = Enumerable.Range(1, 100).Where(IsPrime);
            result.ToList().ForEach(n => Console.WriteLine(n));

            //foreach (var m in mQuery)
            //{
            //    Console.WriteLine(m);
            //}

            //var query = car.Where(c => c.Manufacturer == "BMW" && c.Year == 2016).OrderByDescending(r => r.Combined).ThenBy(r => r.Name) .Select(r=>new {r.Name,r.Combined } );
            //var query2 = from c in car where (c.Manufacturer == "BMW" && c.Year == 2016) orderby c.Combined descending, c.Name select new { c.Name,c.Combined};
            //foreach (var d in query2.Take(10))
            //{
            //    Console.WriteLine(d);
            //}
        }

        private static List<Manufacturer> ProcessManufacturers(string path)
        {
            return File.ReadAllLines(path)
                .Skip(1)
                .Where(line => line.Length > 1).ToManufacturer().ToList();
        }

        private static List<Car> ProcessCars(string path)
        {
            return File.ReadAllLines(path)
                .Skip(1)
                .Where(line => line.Length > 1).ToCar().ToList();
            //.Select(Car.ParseFromCsv)
            //.ToList();
        }

        public static bool StartWithF(Manufacturer m)
        {
            if (m.Name.StartsWith("F"))
            {
                return true;
            }
            return false;
        }

        public static bool IsPrime(int num)
        {
            if (num == 2 || num == 1)
                return true;
            else
            {
                for (int i = 2; i < num - 1; i++)
                {
                    if (num % i == 0)
                        return false;
                }
            }
            return true;
        }
    }
}