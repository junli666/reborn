﻿namespace PluralLinq
{
    internal class Manufacturer
    {
        public string Name { get; set; }
        public string Headquarters { get; set; }
        public int Year { get; set; }

        public override string ToString()
        {
            return $"{ Name,-20}:{ Headquarters,-40}:{Year,-20}";
        }
    }
}