﻿using System;

namespace PluralLinq
{
    internal class Algorithm
    {
        public static int BinarySearch(int[] source, int valueToFind, int start, int end)
        {
            int Pivot = (end + start) / 2;
            if (start == end || start > end)
                return -1;
            else if (valueToFind == source[Pivot])
                return Pivot;
            else if (valueToFind < source[Pivot])
                return BinarySearch(source, valueToFind, start, Pivot);
            else
                return BinarySearch(source, valueToFind, Pivot, end);
        }

        public static int BinarySearchGeneric<T>(T[] source, T valueToFind, int start, int end) where T : IComparable
        {
            int Pivot = (end + start) / 2;
            if (start == end || start > end)
                return -1;
            else if (valueToFind.CompareTo(source[Pivot]) == 0)
                return Pivot;
            else if (valueToFind.CompareTo(source[Pivot]) < 0)
                return BinarySearchGeneric(source, valueToFind, start, Pivot);
            else
                return BinarySearchGeneric(source, valueToFind, Pivot, end);
        }

        public static void InsertSort<T>(T[] source) where T : IComparable
        {
            for (int i = 1; i < source.Length; i++)
            {
                var currentValue = source[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (currentValue.CompareTo(source[j]) < 0)
                    {
                        var temp = source[j];
                        source[j] = currentValue;
                        source[j + 1] = temp; ;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public static void BubbleSort<T>(T[] source) where T : IComparable
        {
            for (int i = 0; i < source.Length; i++)
            {
                for (int j = 0; j < source.Length - 1 - i; j++)
                {
                    if (source[j].CompareTo(source[j + 1]) < 0)
                    {
                        var temp = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = temp;
                    }
                }
            }
        }

        public static void SelectionSort<T>(T[] source) where T : IComparable
        {
            for (int i = 0; i < source.Length; i++)
            {
                int indexOfMin = i;
                for (int j = i; j < source.Length - 1; j++)
                {
                    if (source[indexOfMin].CompareTo(source[j + 1]) > 0)
                    {
                        indexOfMin = j + 1;
                    }
                }
                var temp = source[i];
                source[i] = source[indexOfMin];
                source[indexOfMin] = temp;
            }
        }

        public static void QuickSort<T>(T[] source, int start, int end) where T : IComparable
        {
            var pivot = source[end];
            if (start == end || start > end)
                return;
            else
            {
                int i = Partition(source, start, end);
                QuickSort(source, start, i);
                QuickSort(source, i + 1, end);
            }
        }

        public static int Partition<T>(T[] source, int start, int end) where T : IComparable
        {
            int i = start;
            int j = end - 1;
            T pivot = source[end];
            while (i < j)
            {
                while (source[i].CompareTo(pivot) < 0 && i < j) i++;
                while (source[j].CompareTo(pivot) > 0 && i < j) j--;
                if (i < j)
                {
                    var t = source[i];
                    source[i] = source[j];
                    source[j] = t;
                }
                else
                {
                    source[end] = source[i];
                    source[i] = pivot;
                }
            }
            return j;
        }
    }
}