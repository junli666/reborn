﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PluralLinq
{
    internal static class Extension
    {
        public static IEnumerable<FileInfo> Fileter(this DirectoryInfo di)
        {
            List<FileInfo> a = new List<FileInfo>();
            foreach (var f in di.GetFiles())
            {
                if (f.Length > 500)
                {
                    a.Add(f);
                }
            }

            return a;
        }

        public static IEnumerable<FileInfo> FileterYield(this DirectoryInfo di)
        {
            foreach (var f in di.GetFiles())
            {
                if (f.Length > 500)
                {
                    yield return f;
                }
            }
        }

        public static IEnumerable<T> FilterYield<T>(this IEnumerable<T> source, Predicate<T> predicate)
        {
            foreach (var f in source)
            {
                if (predicate(f))
                    yield return f;
            }
        }

        public static IEnumerable<Manufacturer> ToManufacturer(this IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                var columns = line.Split(',');
                yield return new Manufacturer
                {
                    Name = columns[0],
                    Headquarters = columns[1],
                    Year = int.Parse(columns[2])
                };
            }
        }

        public static IEnumerable<Car> ToCar(this IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                var column = line.Split(',');
                yield return new Car
                {
                    Year = int.Parse(column[0]),
                    Manufacturer = column[1],
                    Name = column[2],
                    Displacement = double.Parse(column[3]),
                    Cylinder = int.Parse(column[4]),
                    City = int.Parse(column[5]),
                    Highway = int.Parse(column[6]),
                    Combined = int.Parse(column[7])
                };
            }
        }
    }
}