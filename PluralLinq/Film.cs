﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PluralLinq
{
    internal class Film
    {
        public int Year { get; set; }
        public string Name { get; set; }
        public double Rating { get; set; }

        public override string ToString()
        {
            return $"{Name,-20}:{Year,-5}:{Rating,-6} ";
        }

        public List<Film> CreateFilmsList()
        {
            List<Film> fList = new List<Film>
            {
                new Film{Name ="Prject A",Rating=7.3,Year=1983},
                new Film{Name ="Wheels on Meals ",Rating=7.2,Year=1984},
                new Film{Name ="Police Story",Rating=7.6,Year=1985},
                new Film{Name ="Drunken Master ",Rating=7.6,Year=1978},
            };
            return fList;
        }

        public void getFilmsAfter1980Lambda()
        {
            var fl = CreateFilmsList().Where(f => f.Year > 1980);
            foreach (var a in fl)
            {
                Console.WriteLine(a);
            }
        }

        public void getFilmsAfter1980Linq()
        {
            var fl = from i in CreateFilmsList() where i.Year > 1980 select i;
            foreach (var a in fl)
            {
                Console.WriteLine(a);
            }
        }

        public void getFilmsAfter1980MyWay()
        {
            var fl = CreateFilmsList().FilterYield(f => f.Year > 1980);
            foreach (var a in fl)
            {
                Console.WriteLine(a);
            }
        }
    }
}